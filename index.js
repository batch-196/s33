const express = require("express");
const app = express();
app.use(express.json())
const port = 4000;

let users = [
	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTofu",
		password: "dubuTofu"
	}
]

let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}
];


app.get('/', (req, res) => {
	res.send("Hello from our first ExpressJS route!");
})

app.post('/', (req, res) => {
	res.send("Hello from our first ExpressJS Post Method Route!")
})


app.put('/', (req, res) => {
	res.send("Hello from a put method route!")
})

app.delete('/', (req, res) => {
	res.send("Hello from a delete method route!")
})


app.get('/courses', (req, res) => {
	res.send(courses);
})

app.post('/courses', (req, res) => {
	//console.log(req.body)
	courses.push(req.body);
	res.send(courses);
})

app.get('/users', (req, res) => {
	res.send(users);
})

app.post('/users', (req, res) => {
	//console.log(req.body)
	users.push(req.body);
	res.send(users);
})

app.delete('/users', (req, res) => {
	users.pop();
	res.send("Last user entry has been deleted")
})

app.listen(port, () => console.log(`Express API running at port 4000`))